// STL
#include <iostream>
#include <ros/ros.h>
#include <pcl/point_cloud.h>
//msgs type and conversion
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
//pcd io
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <message_filters/time_synchronizer.h>
// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/io.h>
#include <typeinfo>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <ros/callback_queue.h>
#include "std_msgs/String.h"

using namespace sensor_msgs;
using namespace message_filters;

ros::CallbackQueue my_callback_queue_;
ros::Publisher pub_pcl_out0, pub_pcl_out1;
// https://github.com/PointCloudLibrary/pcl/blob/master/doc/tutorials/content/sources/matrix_transform/matrix_transform.cpp
void callback(const sensor_msgs::PointCloud2ConstPtr& basePtr, const sensor_msgs::PointCloud2ConstPtr& targetPtr)
{
     std::cout << "callback: " << basePtr->header.stamp << " " << targetPtr->header.stamp <<std::endl;

    /*sensor_msgs::PointCloud2 mess0 = *basePtr;
    sensor_msgs::PointCloud2 mess1 = *targetPtr;
    mess0.header.frame_id = "/livox_base";
    mess1.header.frame_id = "/livox_base";
    pub_pcl_out0.publish(mess0);
    pub_pcl_out1.publish(mess1);
    return;*/

    pcl::PCLPointCloud2 target_pcl_pc2, tmp_xyzin;
    pcl_conversions::toPCL(*targetPtr,target_pcl_pc2);
    pcl_conversions::toPCL(*targetPtr,tmp_xyzin);
    pcl::PointCloud<pcl::PointXYZ>::Ptr target_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZINormal>::Ptr pxyzin_cloud(new pcl::PointCloud<pcl::PointXYZINormal>);
    pcl::fromPCLPointCloud2(target_pcl_pc2,*target_cloud);
    pcl::fromPCLPointCloud2(tmp_xyzin,*pxyzin_cloud);
   

    Eigen::Matrix4f transform_1 = Eigen::Matrix4f::Identity();
    /*transform_1 << 0.972731,  -0.229669,  0.0323386,  -0.580538,
    0.229721,   0.973254,   0.002149,   0.789531,
   -0.0319673, 0.00533844,   0.999475,   0.100438,
    0,          0,          0,          1;*/
    transform_1 << 0.999993, -0.00305849,   0.0020778,  -0.0112149,
 		0.00305359,    0.999993,  0.00235905,    0.694958,
		  -0.002085, -0.00235269,    0.999995,   0.0104302,
			  0,           0,           0,           1;

    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
 
    pcl::transformPointCloud (*target_cloud, *transformed_cloud, transform_1);

    pcl::PointCloud<pcl::PointXYZINormal> cloud_xyzin;
    pcl::copyPointCloud(*transformed_cloud, cloud_xyzin);

  
    for (size_t i = 0; i < cloud_xyzin.points.size(); i++) {
        cloud_xyzin.points[i].intensity = pxyzin_cloud.get()->points[i].intensity;
        cloud_xyzin.points[i].curvature = pxyzin_cloud.get()->points[i].curvature;
    }

    // edit - merging and publishing
    pcl::PCLPointCloud2 base_xyzin;
    pcl_conversions::toPCL(*basePtr, base_xyzin);
    pcl::PointCloud<pcl::PointXYZINormal>::Ptr base_pxyzin_cloud(new pcl::PointCloud<pcl::PointXYZINormal>);
    pcl::fromPCLPointCloud2(base_xyzin, *base_pxyzin_cloud);
    //pcl::PointCloud<pcl::PointXYZINormal> base_xyzin_cloud;
    //pcl_conversions::toPCL(base_xyzin, base_xyzin_cloud);
    // base to XYZIN pcl

    pcl::PointCloud<pcl::PointXYZINormal> mergedCloud;
    
    mergedCloud += (*base_pxyzin_cloud);
    // add base points
    mergedCloud += cloud_xyzin;
    // add transformed points

    sensor_msgs::PointCloud2 pcl_ros_msg;
    pcl::toROSMsg(mergedCloud, pcl_ros_msg);
    pcl_ros_msg.header.stamp.fromNSec(pxyzin_cloud.get()->header.stamp);
    pcl_ros_msg.header.frame_id = "/livox_base"; //pxyzin_cloud.get()->header.frame_id;
    pub_pcl_out0.publish(pcl_ros_msg);
    // publish

    /*pcl::PointCloud<pcl::PointXYZINormal> mergedCloud2;
    mergedCloud2 += cloud_xyzin;
    sensor_msgs::PointCloud2 pcl_ros_msg2;
    pcl::toROSMsg(mergedCloud2, pcl_ros_msg2);
    pcl_ros_msg2.header.stamp.fromNSec(pxyzin_cloud.get()->header.stamp);
    pcl_ros_msg2.header.frame_id = "/livox_base"; //pxyzin_cloud.get()->header.frame_id;
    pub_pcl_out1.publish(pcl_ros_msg2);*/
    // edit end

    /*sensor_msgs::PointCloud2 pcl_ros_msg;
    pcl::toROSMsg(cloud_xyzin, pcl_ros_msg);
    pcl_ros_msg.header.stamp.fromNSec(pxyzin_cloud.get()->header.stamp);

    
    pcl_ros_msg.header.frame_id = pxyzin_cloud.get()->header.frame_id;
    pub_pcl_out0.publish(pcl_ros_msg);
    pub_pcl_out1.publish(basePtr);*/

}

void test(const sensor_msgs::PointCloud2ConstPtr& basePtr, const sensor_msgs::PointCloud2ConstPtr& targetPtr){
   std::cout << "callback: " << basePtr->header.stamp << " " << targetPtr->header.stamp <<std::endl;
   pub_pcl_out0.publish(basePtr);
   pub_pcl_out1.publish(targetPtr);
}


void testCallback1(const sensor_msgs::PointCloud2ConstPtr& basePtr){
   std::cout << "callback 561: " << basePtr->header.stamp <<std::endl;
}
void testCallback2(const sensor_msgs::PointCloud2ConstPtr& basePtr){
   std::cout << "callback 401: " << basePtr->header.stamp <<std::endl;
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "calibration");
  std::cout << "init: " << "calibration" <<std::endl;
  ros::NodeHandle nh;

  // message_filters::Subscriber<sensor_msgs::PointCloud2> sub_livox_msg1(nh, "/livox/lidar_3WEDH7600100561", 30, testCallback1);
  // message_filters::Subscriber<sensor_msgs::PointCloud2> sub_livox_msg2(nh, "/livox/lidar_3WEDH7600107401", 30, testCallback2);
  
  ros::Subscriber sub1 = nh.subscribe("/livox/lidar_3WEDH7600100561", 10, testCallback1);
  ros::Subscriber sub2 = nh.subscribe("/livox/lidar_3WEDH7600107401", 10, testCallback2);
  
  // typedef sync_policies::ApproximateTime<sensor_msgs::PointCloud2, sensor_msgs::PointCloud2> MySyncPolicy;

  // Synchronizer<MySyncPolicy> sync(MySyncPolicy(30), sub_livox_msg1, sub_livox_msg2);

  // sync.registerCallback(boost::bind(&callback, _1, _2));

  // sync.registerCallback(boost::bind(&test, _1, _2));
  // nh.setCallbackQueue(&my_callback_queue_);
  // my_callback_queue_.callOne();

  // pub_pcl_out0 = nh.advertise<sensor_msgs::PointCloud2>("/livox/lidar0", 30);
  // pub_pcl_out1 = nh.advertise<sensor_msgs::PointCloud2>("/livox/lidar1", 30);

  ros::spin();

  return 0;
}
