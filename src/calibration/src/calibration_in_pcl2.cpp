#include <iostream>
#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <message_filters/time_synchronizer.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/io.h>
#include <typeinfo>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include "livox_ros_driver/CustomMsg.h"

using namespace sensor_msgs;
using namespace message_filters;

ros::Publisher pub_pcl_out0, pub_pcl_out1;

typedef pcl::PointXYZINormal PointType;
typedef pcl::PointCloud<PointType> PointCloudXYZI;

uint64_t TO_MERGE_CNT = 1; 
constexpr bool b_dbg_line = false;
std::vector<livox_ros_driver::CustomMsgConstPtr> livox_data;


pcl::PointCloud<pcl::PointXYZINormal>::Ptr LivoxMsgCbk1(const livox_ros_driver::CustomMsgConstPtr& livox_msg_in) {
  livox_data.push_back(livox_msg_in);

  pcl::PointCloud<PointType> pcl_in;

  for (size_t j = 0; j < livox_data.size(); j++) {
    auto& livox_msg = livox_data[j];
    auto time_end = livox_msg->points.back().offset_time;
    for (unsigned int i = 0; i < livox_msg->point_num; ++i) {
      PointType pt;
      pt.x = livox_msg->points[i].x;
      pt.y = livox_msg->points[i].y;
      pt.z = livox_msg->points[i].z;
      float s = livox_msg->points[i].offset_time / (float)time_end;

      pt.intensity = livox_msg->points[i].line +livox_msg->points[i].reflectivity /10000.0 ; 
      pt.curvature = s*0.1;
      pcl_in.push_back(pt);
    }
  }

  pcl::PointCloud<pcl::PointXYZINormal>::Ptr cloudPTR(new pcl::PointCloud<pcl::PointXYZINormal>);
  *cloudPTR = pcl_in;
  livox_data.clear();
  return cloudPTR;
}


 void callback(const livox_ros_driver::CustomMsgConstPtr& basePtr,const livox_ros_driver::CustomMsgConstPtr& targetPtr)
{
    pcl::PointCloud<pcl::PointXYZINormal>::Ptr base_pcl = LivoxMsgCbk1(basePtr);
    pcl::PointCloud<pcl::PointXYZINormal>::Ptr target_pcl = LivoxMsgCbk1(targetPtr);  
    pcl::PointCloud<pcl::PointXYZ> target_cloud;
    pcl::PointCloud<pcl::PointXYZINormal> cloud_xyzi_normal;
    pcl::copyPointCloud(*target_pcl, target_cloud);


    Eigen::Matrix4f transform_1 = Eigen::Matrix4f::Identity();
    transform_1 << 0.999993, -0.00305849,   0.0020778,  -0.0112149,
 		0.00305359,    0.999993,  0.00235905,    0.694958,
		  -0.002085, -0.00235269,    0.999995,   0.0104302,
			  0,           0,           0,           1;

    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::transformPointCloud (target_cloud, *transformed_cloud, transform_1);
    pcl::PointCloud<pcl::PointXYZINormal> cloud_xyzin;
    pcl::copyPointCloud(*transformed_cloud, cloud_xyzin);

    for (size_t i = 0; i < cloud_xyzin.points.size(); i++) {
        cloud_xyzin.points[i].intensity = target_pcl.get()->points[i].intensity;
        cloud_xyzin.points[i].curvature = target_pcl.get()->points[i].curvature;
    }

    pcl::PointCloud<pcl::PointXYZINormal> mergedCloud;
    
    mergedCloud += (*base_pcl);
    mergedCloud += cloud_xyzin;
  
    sensor_msgs::PointCloud2 pcl_ros_msg;
    pcl::toROSMsg(mergedCloud, pcl_ros_msg);
    pcl_ros_msg.header.stamp.fromNSec(base_pcl.get()->header.stamp);
    pcl_ros_msg.header.frame_id = "/livox_base";
    pub_pcl_out0.publish(pcl_ros_msg);
   
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "calibration");
  std::cout << "init: " << "calibration" <<std::endl;
  ros::NodeHandle nh;

  message_filters::Subscriber<livox_ros_driver::CustomMsg> sub_livox_msg1(nh, "/livox/lidar_3WEDH7600100561", 10);
  message_filters::Subscriber<livox_ros_driver::CustomMsg> sub_livox_msg2(nh, "/livox/lidar_3WEDH7600107401", 10);

  typedef sync_policies::ApproximateTime<livox_ros_driver::CustomMsg,livox_ros_driver::CustomMsg> MySyncPolicy;

  Synchronizer<MySyncPolicy> sync(MySyncPolicy(10), sub_livox_msg1, sub_livox_msg2);
  sync.registerCallback(boost::bind(&callback, _1, _2));

  pub_pcl_out0 = nh.advertise<sensor_msgs::PointCloud2>("/livox/lidar0", 10);

  ros::spin();

  return 0;
}
