#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/sakull/cat_ws/src/cmake-build-debug/devel:$CMAKE_PREFIX_PATH"
export PYTHONPATH='/opt/ros/melodic/lib/python2.7/dist-packages:/usr/lib/python3.7/dist-packages'
export ROSLISP_PACKAGE_DIRECTORIES='/home/sakull/cat_ws/src/cmake-build-debug/devel/share/common-lisp'
export ROS_PACKAGE_PATH="/home/sakull/cat_ws/src:$ROS_PACKAGE_PATH"