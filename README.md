

## Dependencies

Livox-SDK [link](https://github.com/Livox-SDK/Livox-SDK)

```sh
git clone https://github.com/Livox-SDK/Livox-SDK.git
cd Livox-SDK
cd build && cmake ..
make
sudo make install
```
Livox-ros-driver [link](https://github.com/Livox-SDK/livox_ros_driver)

```sh
git clone https://github.com/Livox-SDK/livox_ros_driver.git ws_livox/src
```


## How to use it

#### Create catkin workspace and clone repo:
```sh
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
git clone https://DavidSabela@bitbucket.org/DavidSabela/lidar-5g-era.git
cd ..
catkin_make
source ~/catkin_ws/devel/setup.bash

```
#### Start app:
```sh
rosrun tf static_transform_publisher 0.0 0.0 0.0 0.0 0.0 0.0 base/link /livox_base 100
rosbag play *.bag --clock 
roslaunch calibration start_calibration.launch

```


## License

MIT
